function z = xtest(x)
n = length(x);
z = (1:n)./x;
